import { Component, OnInit } from '@angular/core';
import { SalesPerson } from 'src/app/models/SalesPerson';
import { SalesService } from 'src/app/services/sales.service';

@Component({
  selector: 'app-list-sales',
  templateUrl: './list-sales.component.html',
  styleUrls: ['./list-sales.component.css']
})
export class ListSalesComponent implements OnInit {

  //property
  salesTeamMember: SalesPerson[] = []; //initialize as empty array [];

  //dependency injection goes inside constructor
  constructor(private salesSvc: SalesService) { }

  ngOnInit(): void {
    //call the getSales
    this.salesSvc.getSalesTeam().subscribe(
      data => this.salesTeamMember = data
    )

  }

}
